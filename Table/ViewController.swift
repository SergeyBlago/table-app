//
//  ViewController.swift
//  Table
//
//  Created by Sergey Blagodatskikh on 04/07/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//
import Foundation
import UIKit

class ImageTableViewController: UITableViewController {
  let cellIndentifer = "ImageCell"
  var cacheImage = [String: UIImage]()

  override func viewWillAppear(_ animated: Bool) {
    self.tableView.register(ImageCell.self, forCellReuseIdentifier: cellIndentifer)
    super.viewWillAppear(animated)
  }

  override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
    return 100
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIndentifer, for: indexPath)
    guard let url = URL(string: "http://placehold.it/375x150?text=\(indexPath.item+1)") else {
      return cell
    }

    downloadImage(withURL: url, forCell: cell)
    return cell
  }

  func downloadImage(withURL url: URL, forCell cell: UITableViewCell) {

    if let image = cacheImage[url.absoluteString] {
      cell.imageView?.image = image
    } else {
      let task = URLSession.shared.dataTask(with: url) {[ weak self] data, _, _ in
        if let data = data,
          let image = UIImage(data: data) {
          DispatchQueue.main.async {
            self?.cacheImage[url.absoluteString] = image
            cell.imageView?.image = image
          }
        }
      }
      DispatchQueue.global(qos: .utility).async {
        task.resume()
      }
      if let cell = cell as? ImageCell { cell.task = task }
    }
  }
}
