//
//  ImageCell.swift
//  Table
//
//  Created by Sergey Blagodatskikh on 04/07/2019.
//  Copyright © 2019 Sergey Blagodatskikh. All rights reserved.
//

import Foundation
import UIKit

class ImageCell: UITableViewCell {
  var task: URLSessionDataTask?
  override func prepareForReuse() {
    task?.cancel()
    super.prepareForReuse()
  }
}
